package jsymm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

/**
 * Sample project created for showing some concurrency techniques.
 *
 * Created by john on 4/22/17.
 */
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    // Application configuration
    public static final int PRODUCER_COUNT = 5;
    public static final int CONSUMER_COUNT = 2;
    public static final int QUEUE_SIZE = 10;
    public static final long APPLICATION_RUNTIME_SECONDS = 50L;

    // Single thread executor for monitor
    private ExecutorService monitorExecutor;
    private final Monitor monitor;
    private final WidgetStatus widgetStatus;

    // Thread pool for producers set to 5 fixed threads
    private ExecutorService producerPool;
    // Thread pool for consumers set to 2 fixed threads
    private ExecutorService consumerPool;

    Application() {

        // Initialize blocking queue
        final BlockingQueue<Widget> widgetBlockingQueue = new LinkedBlockingQueue<Widget>(QUEUE_SIZE);

        // Initialize new monitor
        this.monitor = new Monitor(widgetBlockingQueue);

        // Initialize widget status
        this.widgetStatus = new WidgetStatus();

        this.monitorExecutor = Executors.newSingleThreadExecutor();
        this.producerPool = Executors.newFixedThreadPool(PRODUCER_COUNT, daemonFactory("producer"));
        this.consumerPool = Executors.newFixedThreadPool(CONSUMER_COUNT, daemonFactory("consumer"));

        // Initialize consumers
        for (int i = 0; i < CONSUMER_COUNT; i++) {
            Consumer consumer = new Consumer(i, widgetBlockingQueue, this.widgetStatus);
            this.monitor.registerConsumer(consumer);
            this.consumerPool.submit(consumer);
        }

        // Initialize producers;
        for (int i = 0; i < PRODUCER_COUNT; i++) {
            Producer producer = new Producer(i, widgetBlockingQueue, this.widgetStatus);
            this.monitor.registerProducer(producer);
            this.producerPool.submit(producer);
        }

        // Start monitor
        this.monitorExecutor.submit(this.monitor);

    }

    // Use daemon threads to allow jvm level fast shutdown
    private ThreadFactory daemonFactory(final String name) {
        return (r) -> {
            Thread t = new Thread(r);
            t.setDaemon(true);
            t.setName(name);
            return t;
        };
    }

    public static void main(String[] args) throws Exception {
        log.info("Starting test application");

        Application application = new Application();

        // As the main thread really has no job until the end, using a sleep.
        // This could of course be implemented many other ways.
        try {
            Thread.sleep(APPLICATION_RUNTIME_SECONDS * 1000L);
        } catch (InterruptedException e) {
            log.warn("Main thread interrupted");
            // Handled in finally below regardless
        } finally {
            log.info("Shutting down test application");

            // Stop the monitor from making rate limit changes
            application.monitor.stopMonitor();

            // Shutdown producers, interrupting existing threads
            application.monitor.stopProducers();
            application.producerPool.shutdownNow(); // Interrupts sleep and exits
            application.producerPool.awaitTermination(5, TimeUnit.SECONDS);
            log.info("Producer pool shut down " + application.producerPool.isShutdown());

            // Shutdown consumers, interrupting existing threads
            application.monitor.stopConsumers();
            application.consumerPool.shutdown(); // Does not interrupt
            application.consumerPool.awaitTermination(60, TimeUnit.SECONDS);
            log.info("Consumer pool shut down " + application.consumerPool.isShutdown());

            // Shutdown monitor executor
            application.monitorExecutor.shutdownNow();

            // Generate status file
            log.info("Outputting widget status to widget-status.txt");
            application.widgetStatus.outputResults();

        }

    }


}
