package jsymm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;

/**
 * Consumer implementation that simulates processing a widget by sleeping for a random period
 * Created by john on 4/22/17.
 */
public class Consumer implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(Consumer.class);

    private final int id;
    private boolean alive = true;
    private final WidgetStatus widgetStatus;

    public int getId() {
        return id;
    }

    private final BlockingQueue<Widget> widgetBlockingQueue;

    public Consumer(final int id, final BlockingQueue<Widget> widgetBlockingQueue, final WidgetStatus widgetStatus) {
        this.id = id;
        this.widgetBlockingQueue = widgetBlockingQueue;
        this.widgetStatus = widgetStatus;
    }

    public void run() {
        log.info("Starting consumer " + this.id);
        try {
            while(alive || this.widgetBlockingQueue.peek() != null) {
                Widget widget = this.widgetBlockingQueue.poll();
                // If we have a widget, simulate processing time
                if (widget != null) {
                    try {
                        long sleepytime = ((long) (Math.random() * 1000)) + 250L;
                        log.info("Processing widget for " + sleepytime);
                        Thread.sleep(sleepytime);
                        this.widgetStatus.widgetConsumed(this.id);
                    } catch (InterruptedException e) {
                        log.info("Consumer " + this.id + " interrupted...");
                        break;
                    }
                }
            }
        } finally {
            log.info("Shutting down consumer " + this.id);
            this.widgetStatus.consumerShutdown(this.id, System.currentTimeMillis());
        }
    }

    public void stop() {
        this.alive = false;
    }

}
