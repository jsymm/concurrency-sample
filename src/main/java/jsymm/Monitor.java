package jsymm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * Monitor specified by requirements to keep queue at only half full by rate limiting the producers
 * Created by john on 4/22/17.
 */
public class Monitor implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(Monitor.class);

    private final BlockingQueue<Widget> widgetBlockingQueue;
    private boolean alive = true;

    private final Map<Integer, Producer> producerMap;
    private final Map<Integer, Consumer> consumerMap;

    public Monitor(final BlockingQueue<Widget> widgetBlockingQueue) {
        this.widgetBlockingQueue = widgetBlockingQueue;
        this.producerMap = new HashMap<>(Application.PRODUCER_COUNT);
        this.consumerMap = new HashMap<>(Application.CONSUMER_COUNT);
    }

    public void run() {
        log.info("Monitoring...");
        int desiredSize = (int) Math.round(Application.QUEUE_SIZE * 0.5);
        log.debug("Desired queue size is " + desiredSize);
        while (alive) {
            try {
                Thread.sleep(1000L);
                log.info("Queue size: " + this.widgetBlockingQueue.size());
                // Be more aggressive when the queue is getting too low
                if (this.widgetBlockingQueue.size() < desiredSize) {
                    adjustProducerRates(this.widgetBlockingQueue.size() - (desiredSize * 2));
                }
                if (this.widgetBlockingQueue.size() > desiredSize) {
                    adjustProducerRates(this.widgetBlockingQueue.size() - desiredSize);
                }
            } catch (InterruptedException e) {
                log.info("Monitor interrupted, shutting down...");
                break;
            }
        }
        log.info("Monitor shut down");
    }

    private void adjustProducerRates(final int factor) {
        for (Producer producer: producerMap.values()) {
            producer.rateAdjustment(factor * 100);
        }
    }

    public void registerProducer(final Producer producer) {
        this.producerMap.put(producer.getId(), producer);
    }

    public void registerConsumer(final Consumer consumer) {
        this.consumerMap.put(consumer.getId(), consumer);
    }

    public void stopProducers() {
        for (Producer producer: producerMap.values()) {
            producer.stop();
        }
    }

    public void stopConsumers() {
        for (Consumer consumer: consumerMap.values()) {
            consumer.stop();
        }
    }

    public void stopMonitor() {
        this.alive = false;
    }

}
