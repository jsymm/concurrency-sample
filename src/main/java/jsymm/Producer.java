package jsymm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;

/**
 * Produces widgets in a rate limited fashion for consumption by Consumer
 * Created by john on 4/22/17.
 *
 * @see Consumer
 */
public class Producer implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(Producer.class);

    private final BlockingQueue<Widget> widgetBlockingQueue;
    private final WidgetStatus widgetStatus;
    private final int id;
    private boolean alive = true;

    private long productionRateMs = 1000L;

    public int getId() {
        return id;
    }

    public Producer(final int id, final BlockingQueue<Widget> widgetBlockingQueue, final WidgetStatus widgetStatus) {
        this.id = id;
        this.widgetBlockingQueue = widgetBlockingQueue;
        this.widgetStatus = widgetStatus;
    }

    public void run() {
        log.info("Starting producer " + this.id);
        try {
            while (alive) {
                Widget widget = new Widget();
                try {
                    this.widgetBlockingQueue.put(widget);
                    this.widgetStatus.widgetProduced(this.id);
                    log.info("Publishing widget from " + this.id);
                    // Break into multiple sleeps to be more responsive to timing changes
                    Thread.sleep(this.productionRateMs / 4L);
                    Thread.sleep(this.productionRateMs / 4L);
                    Thread.sleep(this.productionRateMs / 4L);
                    Thread.sleep(this.productionRateMs / 4L);
                } catch (InterruptedException e) {
                    log.info("Producer " + this.id + " interrupted...");
                    break;
                }
            }
        } finally {
            log.info("Shutting down producer " + this.id);
            this.widgetStatus.producerShutdown(this.id, System.currentTimeMillis());
        }
    }

    public void rateAdjustment(long adjustment) {
        long newRate = this.productionRateMs + adjustment;
        this.productionRateMs = newRate > 10L ? newRate : 10L;
        this.widgetStatus.rateDelay(this.id, this.productionRateMs);
        log.trace("Adjusting rate for producer " + id + " by " + adjustment + " now " + this.productionRateMs);
    }

    public void stop() {
        this.alive = false;
    }

}
