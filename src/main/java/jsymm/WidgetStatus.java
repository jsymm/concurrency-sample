package jsymm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicLongArray;

/**
 * This tracks the status of all widgets processed
 * Created by john on 4/22/17.
 */
public class WidgetStatus {

    private static final Logger log = LoggerFactory.getLogger(WidgetStatus.class);

    private final AtomicInteger totalWidgetsProduced = new AtomicInteger();
    private final AtomicInteger totalWidgetsConsumed = new AtomicInteger();

    private final AtomicIntegerArray producerWidgets = new AtomicIntegerArray(Application.PRODUCER_COUNT);
    private final AtomicIntegerArray consumerWidgets = new AtomicIntegerArray(Application.CONSUMER_COUNT);

    private final AtomicLongArray producerRateDelays = new AtomicLongArray(Application.PRODUCER_COUNT);
    private final AtomicLongArray producerShutdowns = new AtomicLongArray(Application.PRODUCER_COUNT);
    private final AtomicLongArray consumerShutdowns = new AtomicLongArray(Application.CONSUMER_COUNT);

    public void rateDelay(final int id, final long delay) {
        producerRateDelays.set(id, delay);
    }

    public void producerShutdown(final int id, final long ts) {
        producerShutdowns.set(id, ts);
    }

    public void consumerShutdown(final int id, final long ts) {
        consumerShutdowns.set(id, ts);
    }

    public void widgetConsumed(final int id) {
        consumerWidgets.getAndIncrement(id);
        totalWidgetsConsumed.getAndIncrement();
    }

    public void widgetProduced(final int id) {
        producerWidgets.getAndIncrement(id);
        totalWidgetsProduced.getAndIncrement();
    }

    /**
     * Dump collected status to a text file
     * @throws Exception
     */
    public void outputResults() throws Exception {
        File output = new File("widget-status.txt");
        if (output.exists() && output.delete()) {
            log.info("Resetting widget-status.txt file");
        }
        if (output.createNewFile()) {
            FileWriter writer = new FileWriter(output);
            writer.write("Widget Status Results\n");
            writer.write("---------------------\n");

            writer.write("\nWidgets produced by producer:\n");
            for (int i = 0; i < Application.PRODUCER_COUNT; i++) {
                writer.write(" - Producer " + i + " produced " + producerWidgets.get(i) + " widgets.\n");
            }

            writer.write("\nTotal widgets produced: " + totalWidgetsProduced.get() + "\n");

            writer.write("\nProducer delays:\n");
            for (int i = 0; i < Application.PRODUCER_COUNT; i++) {
                writer.write(" - Producer " + i + " had a rate delay set to " + producerRateDelays.get(i) + "ms.\n");
            }

            writer.write("\nWidgets consumed by consumer:\n");
            for (int i = 0; i < Application.CONSUMER_COUNT; i++) {
                writer.write(" - Consumer " + i + " consumed " + consumerWidgets.get(i) + " widgets.\n");
            }

            writer.write("\nTotal widgets consumed: " + totalWidgetsConsumed.get() + "\n");

            writer.write("\nThread stop times:\n");
            for (int i = 0; i < Application.PRODUCER_COUNT; i++) {
                writer.write(" - Producer " + i + " stopped at " + producerShutdowns.get(i) + ".\n");
            }
            for (int i = 0; i < Application.CONSUMER_COUNT; i++) {
                writer.write(" - Consumer " + i + " stopped at " + consumerShutdowns.get(i) + ".\n");
            }

            writer.flush();
            writer.close();
        }
    }


}
